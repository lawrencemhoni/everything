<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dataset extends Model
{
    use HasFactory;

    function fields () {
        return $this->belongsToMany(Field::class, 'dataset_field', 'dataset_id', 'field_id');
    }


    public  function scopeDatasetIdHasFieldName($query, $datasetId, $fieldName) {

        $count = $query->select()
                       ->join('dataset_field', 'dataset_field.dataset_id', 'datasets.id')
                       ->join('fields', 'fields.id', 'dataset_field.field_id')
                       ->where('dataset_field.dataset_id', $datasetId)
                       ->where('fields.name', $fieldName)
                       ->count();

        return $count > 0;

    }

}
