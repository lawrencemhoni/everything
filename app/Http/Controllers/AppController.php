<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Exception;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\ForbiddenAccessException;

use App\RequestModelManagers\AppManager;

use App\Services\EntityAccessService;


class AppController extends Controller
{
    public function __construct (EntityAccessService $entityAccessServices) {
        $this->entityAccess = $entityAccessServices;
    }

    public function createApp (Request  $req) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('app',
            'create_activity_type');

            $app = AppManager::createApp($req);

            return response()->json(['data' => $app], 201);

        }  catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }
    }


    public function getApps (Request $req) {

        try {

            $this->entityAccess->check('app',
            'list_activity_type');

            return AppManager::getApps($req);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }
    }

    public function viewApp (Request $req, $id) {

        try {

            $this->entityAccess->check('app',
            'view_activity_type');

            $app = AppManager::getApp($req, $id);

            return response()->json(['data' => $app], 200);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException ) {
            return response()->json([], 404);
        } catch ( Exception ) {
            return response()->json([], 500);
        }
    }

    public function updateApp (Request $req, $id) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('app',
            'update_activity_type');

            $app = AppManager::updateApp($req, $id);

            return response()->json(['data' => $app], 200);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException ) {
            return response()->json([], 404);
        } catch ( Exception ) {
            return response()->json([], 500);
        }

    }



}
