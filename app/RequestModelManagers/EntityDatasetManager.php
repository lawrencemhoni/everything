<?php

namespace App\RequestModelManagers;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\EntityDataset;
use App\Models\Field;
use App\Exceptions\RecordConflictException;
use App\Exceptions\InvalidDataException;

use DB;


class EntityDatasetManager extends RequestModelManager {

    /**
     * Create dataset
     * @param $req : Instance of the current request
     * @return Dataset : created dataset
     */
     public static function createEntityDataset (Request $req, $entityId) : EntityDataset
     {

        $entity = self::getValidEntityByIdOrFail($entityId);

        if (self::EntityDatasetExists($entity->id, $req->name)) {
            $msg = "Dataset ({$req->name}) already exists.";
            throw new RecordConflictException($msg);
        }

        $entityDataset = new EntityDataset;
        $entityDataset->name = $req->name;
        $entityDataset->entity_id = $entity->id;
        $entityDataset->save();

        if (self::arrayHasItems($req->fields)) {

            self::attachEntityDatasetFieldsFromArray($entityDataset, $req->fields);

        }

        return self::getEntityDatasetWithRelationshipsByIdOrFail($entity->id, $entityDataset->id);
     }


    /**
     * Get datasets
     * @param $req : Instance of the current request
     * @return Dataset : a collection of datasets
     */
     public static function getEntityDatasets (Request $req, $entityId) : Paginator
     {

        $entityDatasets = EntityDataset::select(DB::raw('DISTINCT entity_datasets.id'),
                                                'entity_datasets.name',
                                                'entity_datasets.entity_id')
                                        ->join('entities', 'entities.id', 'entity_datasets.entity_id')
                                        ->where('entities.id', $entityId);


        if ($req->search_query) {
            $entityDatasets->where('entity_datasets.name', 'LIKE', "%%{$req->search_query}%%");
        }

        return $entityDatasets->paginate();

     }


    /**
     * Get dataset
     * @param $req : Instance of the current request
     * @param $id : ID of the dataset
     * @return Dataset: found dataset
     */
     public static function getEntityDataset (Request $req, $entityId, $id) : EntityDataset
     {
        $entity = self::getValidEntityByIdOrFail($entityId);

        return self::getEntityDatasetWithRelationshipsByIdOrFail($entity->id, $id);

     }


    /**
     * Update dataset
     * @param $req : Instance of the current request
     * @param $id : ID of the dataset
     * @return Dataset : updated dataset
     */
     public static function updateEntityDataset (Request $req, $entityId, $id) : EntityDataset
     {

        $entity = self::getValidEntityByIdOrFail($entityId);


        if (self::entityDatasetExistsExceptForId($entity->id, $req->name, $id)) {
            $msg = "Entity dataset ({$req->name}) already exists.";
            throw new RecordConflictException($msg);
        }

        $entityDataset = self::getEntityDatasetByIdOrFail($entity->id, $id);

        $entityDataset->name = $req->name;
        $entityDataset->entity_id = $entity->id;
        $entityDataset->save();

        if (self::arrayHasItems($req->fields)) {

            self::attachEntityDatasetFieldsFromArray($entityDataset, $req->fields);

        }

        return self::getEntityDatasetWithRelationshipsByIdOrFail($entity->id, $id);

     }



     public static function attachEntityDatasetFieldsFromArray ($dataset, $fieldsArray) {

        foreach ($fieldsArray as $fieldItem) {

            $fieldType;

            $typeCategoryKey = 'field_types_category';

            if (isset($fieldItem['type_key'])) {

                $fieldKey =  $fieldItem['type_key'];

                $fieldType = self::getCategoryTypeByKey($fieldKey, $typeCategoryKey);

                if (!$fieldType) {
                    continue;
                }

            } else if (isset($fieldItem['type_id'])) {

                $typeId = $fieldItem['type_id'];

                $fieldType = self::getCategoryTypeById($typeId, $typeCategoryKey);

                if (!$fieldType) {
                    continue;
                }
            }

            $field = null;

            if(isset($fieldItem['id'])) {
                $field = Field::select()
                             ->where('id', $fieldItem['id'])
                             ->first();

            } else if (self::datasetFieldExists($dataset->id, $fieldItem['name'] )) {
                continue;
            }

            $field = ($field)? $field : new Field;
            $field->name = $fieldItem['name'];
            $field->type_id = $fieldType->id;
            $field->save();

            if (!self::datasetHasField ($dataset->id, $field->id)) {
                $dataset->fields()->attach([$field->id]);
            }

        }

     }



    /**
     * Check if dataset exists
     * @param $name : name of the dataset
     * @param $typeKey : key of the dataset type
     * @return Bool
     */
     protected static function entityDatasetExists ($entityId, $name)
     {

        $count = EntityDataset::where('name', $name)
                              ->where('entity_id', $entityId)
                              ->count();

        return $count > 0;

     }


    /**
     * Check if dataset exists except for where there is a specific ID
     * @param $name : name of the dataset
     * @param $typeKey : key of the dataset type
     * @return Bool
     */
     protected static function entityDatasetExistsExceptForId ($entityId, $name, $id )
     {

        $count = EntityDataset::where('name', $name)
                              ->where('entity_id', $entityId)
                              ->where('id', '!=', $id)
                              ->count();

        return $count > 0;

     }


    /**
     * Get dataset by ID or throw an exception
     * @return Dataset : a found dataset
     */
     protected static function getEntityDatasetByIdOrFail ($entityId, $id) : EntityDataset
     {

        $dataset = EntityDataset::select()
                                ->where('entity_id', $entityId)
                                ->where('id', $id)
                                ->firstOrFail();

        return $dataset;

     }


     public static function datasetFieldExists ($datasetId, $fieldName) : bool
     {

        $count = Field::join('entity_dataset_field', 'entity_dataset_field.field_id', 'fields.id')
                     ->where('fields.name', $fieldName)
                     ->where('entity_dataset_field.entity_dataset_id', $datasetId)
                     ->count();

        return $count > 0;

     }



     public static function datasetHasField ($datasetId, $fieldId) : bool
     {

        $count = Field::join('entity_dataset_field', 'entity_dataset_field.field_id', 'fields.id')
                     ->where('fields.id', $fieldId)
                     ->where('entity_dataset_field.entity_dataset_id', $datasetId)
                     ->count();

        return $count > 0;

     }



    /**
     * Get dataset with relationships by ID or throw an exception
     * @return EntityDataset : a found dataset
     */
     protected static function getEntityDatasetWithRelationshipsByIdOrFail ($entityId, $id) : EntityDataset
     {

        $dataset = EntityDataset::select(DB::raw('DISTINCT entity_datasets.id'),
                         'entity_datasets.name', 'entity_datasets.entity_id')
                          ->where('entity_datasets.id', $id)
                          ->with('fields')
                          ->firstOrFail();

        return $dataset;

     }








}
