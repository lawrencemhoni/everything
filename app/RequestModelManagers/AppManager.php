<?php

namespace App\RequestModelManagers;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Illuminate\Http\Request;
use App\Models\App;
use App\Exceptions\RecordConflictException;
use App\Exceptions\InvalidDataException;


class AppManager extends RequestModelManager {


    /**
     * Create app
     * @param $req : Instance of the current request
     * @return App : created app
     */
    public static function createApp (Request $req) : App
    {

        $type = self::getTypeByKeyOrFail($req->type_key);

        if (self::appExists($req->name, $type->id)) {
            $msg = "App ({$req->name}) already exists.";
            throw new RecordConflictException($msg);
        }

        $app = new App;
        $app->name = $req->name;
        $app->type_id = $type->id;
        $app->api_key = $req->api_key;
        $app->remote_uid = $req->uid;
        $app->save();

        return $app;
    }

    /**
     * Get apps
     * @param $req : Instance of the current request
     * @return App : a collection of apps
     */
     public static function getApps (Request $req) : Paginator
     {
        $apps = App::select();

        if ($req->search_query) {
            $apps->where('name', 'LIKE', "%%{$req->search_query}%%");
        }

        return $apps->paginate();
     }


    /**
     * Create app
     * @param $req : Instance of the current request
     * @param $id : ID of the app
     * @return App : found app
     */
     public static function getApp (Request $req, $id) : App
     {
        $app = App::select()
                  ->where('id', $id)
                  ->firstOrFail();
        return $app;
     }

    /**
     * Update app
     * @param $req : Instance of the current request
     * @param $id : ID of the app
     * @return App : updated app
     */
    public static function updateApp (Request $req, $id) : App
    {

        $type = self::getTypeByKeyOrFail($req->type_key);

        if (self::appExistsExceptForId($id, $req->name, $type->id)) {
            $msg = "App ({$req->name}) already exists.";
            throw new RecordConflictException($msg);
        }


        $app = App::select()
                    ->where('id', $id)
                    ->firstOrFail();

        $app->name = $req->name;
        $app->type_id = $type->id;
        $app->api_key = $req->api_key;
        $app->remote_uid = $req->uid;
        $app->save();

        return $app;
    }

    /**
     * Check if app exists
     * @param $name : name of the app to be checked
     * @return Bool
     */
    protected static function appExists ($name, $typeId) : bool
    {
        $count = App::where('name', $name)
                    ->where('type_id', $typeId)
                    ->count();

        return $count > 0;
    }

    /**
     * Check if app exists where ID is not equal to the given $id
     * @param $id : ID of the app
     * @param $name : name of the app to be checked
     * @return Bool
     */
    protected static function appExistsExceptForId ($id, $name, $typeId) : bool
    {
        $count = App::where('apps.name', $name)
                    ->where('type_id', $typeId)
                    ->where('id', '!=', $id)
                    ->count();

        return $count > 0;
    }


}
