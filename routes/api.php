<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\EntityController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PlatformController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DatasetController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('platform-zero')->group(function(){


    Route::controller(AuthController::class)
         ->prefix('auth')
         ->group(function () {

        Route::post('login', 'login');
        Route::get('logout', 'logout')
             ->middleware('auth');

    });

    Route::middleware('auth')->group(function () {


        Route::controller(EntityController::class)
            ->prefix('entities')
            ->group(function () {

            Route::post('/{entityId}/datasets/create', 'createEntityDataset');

            Route::post('/{entityId}/datasets/{datasetId}/update', 'updateEntityDataset');

            Route::get('/{id}/datasets', 'getEntityDatasets');
            Route::get('/{entityId}/datasets/{datasetId}', 'viewEntityDataset');

        });



        Route::controller(PlatformController::class)
            ->prefix('platforms')
            ->group(function () {

            Route::post('/create', 'createPlatform');
            Route::post('/{id}/update', 'updatePlatform');

            Route::get('/', 'getPlatforms');
            Route::get('/{id}', 'viewPlatform');

        });


        Route::controller(AppController::class)
                ->prefix('apps')
                ->group(function () {

            Route::post('/create', 'createApp');
            Route::post('/{id}/update', 'updateApp');

            Route::get('/', 'getApps');
            Route::get('/{id}', 'viewApp');

        });


        Route::controller(UserController::class)
                ->prefix('users')
                ->group(function () {

            Route::post('/create', 'createUser');
            Route::post('/{id}/update', 'updateUser');

            Route::get('/', 'getUsers');
            Route::get('/{id}', 'viewUser');

        });

        Route::controller(TypeController::class)
            ->prefix('types')
            ->group(function () {

            Route::post('/create', 'createType');
            Route::post('/{id}/update', 'updateType');

            Route::get('/', 'getTypes');

            Route::get('/{id}', 'viewType');

        });



        Route::controller(CategoryController::class)
            ->prefix('categories')
            ->group(function () {

            Route::post('/create', 'createCategory');
            Route::post('/{id}/update', 'updateCategory');

            Route::get('/', 'getCategories');

            Route::get('/{id}', 'viewCategory');

        });



        Route::controller(DatasetController::class)
                ->prefix('datasets')
                ->group(function () {

            Route::post('/create', 'createDataset');
            Route::post('/{id}/update', 'updateDataset');

            Route::get('/', 'getDatasets');

            Route::get('/{id}', 'viewDataset');

        });







    });





});

