<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Testing\Fluent\AssertableJson;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Dataset;
use App\Models\Field;

class DatasetTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * Create Dataset Test
     * @test
     */
    public function user_can_create_dataset () : void
    {

        $url = 'platform-zero/datasets/create';

        $data = [
            'name' => 'Appliances Data',
            'type_key' => 'entry_dataset_type'
        ];

        $response1 = $this->actingAsTestUser()->postJson($url, $data);

        $response1->assertStatus(201)
                 ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                        ->etc()
                );

    }

    /**
     * Create Dataset with Fields Test
     * @test
     */
    public function user_can_create_dataset_with_fields () : void
    {


        $url = 'platform-zero/datasets/create';

        $data = [
            'name' => 'Appliances Data',
            'type_key' => 'entry_dataset_type'
        ];


        #Now we are testing if JSON has fields

        $fields = Field::factory()->count(2)->create()->toArray();

        $fieldsArr = array_map(function ($item) {

            return [
                'name' => $item['name'],
                'type_id' => $item['type_id']
            ];

        }, $fields);


        $data['fields'] = $fieldsArr;

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertJsonStructure([
            'data' => [
                    'id',
                    'name',
                    'fields' => [
                        [
                            'id',
                            'name',
                            'type_id'
                        ]

                    ]
                ]

        ]);

    }



    /**
     * Get Dataset Test
     * @test
     */
    public function user_can_get_dataset () : void
    {

        $dataset = Dataset::factory()->create();

        $url = "/platform-zero/datasets/{$dataset->id}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('data.id', $dataset->id)
                 ->where('data.name', $dataset->name)
                 ->etc()
        );


    }


   /**
     * Get Apps Test
     * @test
     */
    public function user_can_get_datasets () : void
    {

        Dataset::factory()->count(20)->create();

        $url = '/platform-zero/datasets';

        $response = $this->actingAsTestUser()->getJson($url);


        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ]);
    }


   /**
     *Update Dataset Test
     *@test
     */
    public function user_can_update_dataset () : void
    {
        $dataset = Dataset::factory()->create();

        $url = "/platform-zero/datasets/{$dataset->id}/update";

        $data = [
            'name' => 'Vehicles Data',
            'type_key' => 'entry_dataset_type'
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.name', $data['name'])
                    ->etc()
        );




    }



   /**
     *Update Dataset with Field Test
     *@test
     */
    public function user_can_update_dataset_with_fields () : void
    {

        $dataset = Dataset::factory()->create();

        $url = "/platform-zero/datasets/{$dataset->id}/update";

        $data = [
            'name' => 'Motor Parts Data',
            'type_key' => 'entry_dataset_type'
        ];

        $fields = Field::factory()->count(2)->create()->toArray();

        $fieldsArr = array_map(function ($item) {

            return [
                'name' => $item['name'],
                'type_id' => $item['type_id']
            ];

        }, $fields);

        $data['fields'] = $fieldsArr;

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertJsonStructure([
            'data' => [
                    'id',
                    'name',
                    'fields' => [
                        [
                            'id',
                            'name',
                            'type_id'
                        ]

                    ]
                ]

        ]);

    }









}
