<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\ForbiddenAccessException;

use App\RequestModelManagers\DatasetManager;

use App\Services\EntityAccessService;

use Exception;


class DatasetController extends Controller
{

    public function __construct (EntityAccessService $entityAccessServices) {
        $this->entityAccess = $entityAccessServices;
    }

    public function createDataset (Request $req) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('dataset',
            'create_activity_type');

            $dataset = DatasetManager::createDataset($req);

            return response()->json(['data' => $dataset], 201);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception $e ) {
            \Log::info($e->getMessage());
            return response()->json([], 500);
        }

    }

    public function getDatasets (Request $req) {

        try {

            $this->entityAccess->check('dataset',
            'list_activity_type');

            return DatasetManager::getDatasets($req);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception $e ) {
            return response()->json([], 500);
        }

    }


    public function viewDataset (Request $req, $id) {

        try {

            $this->entityAccess->check('dataset',
            'view_activity_type');

            $dataset = DatasetManager::getDataset($req, $id);

            return response()->json(['data' => $dataset], 200);


        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception  $e) {
            return response()->json([], 500);
        }
    }


    public function updateDataset (Request $req, $id) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('dataset',
            'update_activity_type');

            $dataset =  DatasetManager::updateDataset($req, $id);

            return response()->json(['data' => $dataset], 200);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        }  catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException $e) {
            return response()->json([], 404);
        } catch ( Exception $e) {
            \Log::info($e->getMessage());
            return response()->json([], 500);
        }

    }



}
