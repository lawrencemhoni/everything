<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entity_context_dataset_field_option_value', function (Blueprint $table) {
            $table->id();
            $table->integer('entity_id');
            $table->integer('subject_id');
            $table->integer('dataset_id');
            $table->integer('field_id');
            $table->integer('option_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entity_context_dataset_field_option_value');
    }
};
