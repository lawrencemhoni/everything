<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    use HasFactory;
    
    public function apps () {
        return $this->belongsToMany(App::class, 'platform_app', 'platform_id', 'app_id');
    }

}
