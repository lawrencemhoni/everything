<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


use App\Models\Entity;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EntityDataset>
 */
class EntityDatasetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $entityIds = Entity::select('entities.id')
                            ->filterByTypeKey('public_entity_type')
                            ->get()
                            ->pluck('id')
                            ->toArray();


        $rand = array_rand($entityIds, 1);
        $key = is_array($rand)? array_shift($rand) : $rand ;

        return [
            'name' => fake()->name(),
            'entity_id' => $entityIds[$key],
        ];
    }
}
