<?php

namespace App\RequestModelManagers;

use App\Models\Type;
use App\Models\Entity;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;

use DB;


class RequestModelManager {


    protected static function typeKeyNotUnauthorized ($key) {
        return !in_array($key, self::$unauthorizedTypeKeys);
    }


    /**
     * Get Entity with the given key
     * @param $key : key of the type
     * @return Entity : a found type
     */
    protected static function geEntityByKey ($key) : Entity
    {

       return Entity::select()->where('key', $key)->first();

    }


    /**
     * Get Entity with the given key or throw an exception
     * @param $key : key of the entity
     * @return Entity : a found entity
     */
    protected static function getEntityByKeyOrFail ($key) : Entity
    {

        $entity = self::geEntityByKey($key);

        if (!$entity ) {
            $msg = "Invalid entity key ({$key} given)";
            throw new InvalidDataException($msg);
        }

        return  $entity;

    }



    /**
     * Get Entity with the given key
     * @param $id : id of the entity
     * @return Entity : a found entity
     */
    protected static function getValidEntityById ($id) : Entity
    {

       $entity = Entity::select(DB::raw('DISTINCT entities.id'),
                                'entities.name', 'entities.key',
                                'entities.type_id')
                        ->filterByTypeKey('public_entity_type')
                        ->where('entities.id', $id)
                        ->first();

        return $entity;

    }


    /**
     * Get Entity with the given id or throw an exception
     * @param $id : id of the type
     * @return Entity : a found type
     */
    protected static function getValidEntityByIdOrFail ($id) : Entity
    {

        $entity = self::getValidEntityById($id);

        if (!$entity ) {
            $msg = "Invalid entity id ({$id} given)";
            throw new InvalidDataException($msg);
        }

        return  $entity;

    }




    /**
     * Get type with the given key
     * @param $key : key of the type
     * @return Type : a found type
     */
    protected static function getTypeByKey ($key) : Type
    {

       return Type::select()->where('key', $key)->firstOrFail();

    }


    /**
     * Get type with the given key or throw an exception
     * @param $key : key of the type
     * @return Type : a found type
     */
    protected static function getTypeByKeyOrFail ($key) : Type
    {

        $type = Type::select()
                    ->where('key', $key)
                    ->first();

        if (!$type) {
            $msg = "Invalid type key ({$key} given)";
            throw new InvalidDataException($msg);
        }

        return $type;

    }

    /**
     * Get type with the given key
     * @param $key : key of the type
     * @return Type : a found type
     */
    protected static function getCategoryTypeById ($typeId, $categoryKey) : Type
    {

       return Type::select(DB::raw('DISTINCT types.id'), 'types.name', 'types.key')
                 ->join('categories', 'categories.id', 'types.category_id')
                 ->where('types.id', $typeId)
                 ->where('categories.key', $categoryKey)
                 ->first();

    }


    /**
     * Get type with the given key
     * @param $key : key of the type
     * @return Type : a found type
     */
    protected static function getCategoryTypeByKey ($typeKey, $categoryKey) : Type
    {

        return Type::select(DB::raw('DISTINCT types.id'), 'types.name', 'types.key')
                    ->join('categories', 'categories.id', 'types.category_id')
                    ->where('types.key', $typeKey)
                    ->where('categories.key', $categoryKey)
                    ->first();

    }




    /**
     * Check if the value is an array and if it has items
     * @param $arr : a value that is ideally supposed to be an array
     * @return bool
     */
    protected static function arrayHasItems ($arr) : bool
    {

        if ($arr && is_array($arr) && count($arr)) {
            return true;
        }

        return false;

    }

}
