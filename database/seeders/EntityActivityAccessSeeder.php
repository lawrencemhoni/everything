<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Entity;
use App\Models\Type;
use App\Models\EntityActivityAccess;


class EntityActivityAccessSeeder extends Seeder
{

    private $entityActivityAccesses = [


        'system' => [
            'update_activity_type',
        ],

        'entity' => [],

        'status' => [],

        'mode' => [],

        'option' => [],

        'access' => [],

        'session' => [],

        'platform' => [
            'create_activity_type',
            'list_activity_type',
            'view_activity_type',
            'search_activity_type',
            'update_activity_type',
            'delete_activity_type',
        ],


        'app' => [
            'create_activity_type',
            'list_activity_type',
            'view_activity_type',
            'search_activity_type',
            'update_activity_type',
            'delete_activity_type',
        ],


        'user' => [
            'create_activity_type',
            'list_activity_type',
            'view_activity_type',
            'search_activity_type',
            'update_activity_type',
            'delete_activity_type',
        ],


        'type' => [
            'create_activity_type',
            'list_activity_type',
            'view_activity_type',
            'search_activity_type',
            'update_activity_type',
            'delete_activity_type',
        ],

        'category' => [
            'create_activity_type',
            'list_activity_type',
            'view_activity_type',
            'search_activity_type',
            'update_activity_type',
            'delete_activity_type',
        ],


        'dataset' => [
            'create_activity_type',
            'list_activity_type',
            'view_activity_type',
            'search_activity_type',
            'update_activity_type',
            'delete_activity_type',
        ],

        'entity_dataset' => [
            'create_activity_type',
            'list_activity_type',
            'view_activity_type',
            'search_activity_type',
            'update_activity_type',
            'delete_activity_type',
        ],


        'field' => [],

        'file' => [
            'create_activity_type',
            'list_activity_type',
            'view_activity_type',
            'search_activity_type',
            'update_activity_type',
            'delete_activity_type',
            'copy_activity_type',
            'move_activity_type',
            'upload_activity_type',
            'download_activity_type',
        ],



    ];


    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->entityActivityAccesses as $entityKey => $typeIds ) {
			foreach ($typeIds as $typeKey) {

                $entity = Entity::where('key', $entityKey)
                            ->firstOrFail();

                $type = Type::where('key', $typeKey)
                            ->firstOrFail();

                $entityActivityAccess = EntityActivityAccess::firstOrCreate(
                            [
                                'entity_id' => $entity->id,
                                'type_id' => $type->id,
                            ]
                );

			}

		}
    }
}
