<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    use HasFactory;

    public function platforms () {
        return $this->belongsToMany(Platform::class, 'platform_id', 'app_id', 'platform_app');
    }
}
