<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Entity extends Model
{
    use HasFactory;


    public function scopeSelectBasics ($query) {

        return $query->select(DB::raw('DISTINCT entities.id'),
        'entities.name', 'entities.key',
        'entities.type_id');

    }

    public function scopeFilterByTypeKey ($query, $typeKey) {

        return $query->join('types', 'types.id', 'entities.type_id')
                     ->where('types.key', $typeKey);

     }

}
