<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    use HasFactory;

    public  function scopeDatasetIdHasFieldName($query, $datasetId, $fieldName) {

        $count = $query->select()
                       ->join('dataset_field', 'dataset_field.field_id', 'fields.id')
                       ->where('dataset_field.dataset_id', $datasetId)
                       ->where('fields.name', $fieldName)
                       ->count();

        return $count > 0;

    }
}
