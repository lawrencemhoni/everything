<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Entity;
use App\Models\Type;

class EntitySeeder extends Seeder
{

    private $entities = [

        [
            'name' => 'System',
            'key' => 'system',
            'entity_type_key' => 'private_entity_type'
        ],

        [
            'name' => 'Context',
            'key' => 'context',
            'entity_type_key' => 'abstract_entity_type'
        ],


        [
            'name' => 'Session',
            'key' => 'session',
            'entity_type_key' => 'private_entity_type'
        ],

        [
            'name' => 'Access',
            'key' => 'access',
            'entity_type_key' => 'private_entity_type'
        ],


        [
            'name' => 'Entity',
            'key' => 'entity',
            'entity_type_key' => 'private_entity_type'
        ],
        [
            'name' => 'Status',
            'key' => 'status',
            'entity_type_key' => 'private_entity_type'
        ],
        [
            'name' => 'Mode',
            'key' => 'mode',
            'entity_type_key' => 'private_entity_type'
        ],
        [
            'name' => 'Category',
            'key' => 'category',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'Type',
            'key' => 'type',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'Option',
            'key' => 'option',
            'entity_type_key' => 'private_entity_type'
        ],

        [
            'name' => 'Entry',
            'key' => 'entry',
            'entity_type_key' => 'public_entity_type'
        ],

        [
            'name' => 'Dataset',
            'key' => 'dataset',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'Entity Dataset',
            'key' => 'entity_dataset',
            'entity_type_key' => 'public_entity_type'
        ],

        [
            'name' => 'Field',
            'key' => 'field',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'Platform',
            'key' => 'platform',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'App',
            'key' => 'app',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'File',
            'key' => 'file',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'Access Level',
            'key' => 'access_level',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'User',
            'key' => 'user',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'Account',
            'key' => 'account',
            'entity_type_key' => 'public_entity_type'
        ],
        [
            'name' => 'Organization',
            'key' => 'organization',
            'entity_type_key' => 'public_entity_type'
        ]


    ];


    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->entities as $item) {

            $type = Type::where('key', $item['entity_type_key'])
                        ->firstOrFail();

            $entity = Entity::firstOrCreate(
                ['key' => $item['key']],
                [
                    'key' => $item['key'],
                    'name' => $item['name'],
                    'type_id' => $type->id
                ]
            );

        }
    }
}
