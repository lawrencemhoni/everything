<?php

namespace App\RequestModelManagers;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Hash;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserEntityActivityAccess;
use App\Exceptions\RecordConflictException;
use App\Exceptions\InvalidDataException;



class UserManager extends RequestModelManager {

    /**
     * Create user
     * @param $req : Instance of the current request
     * @return User : created user
     */
    public static function  createUser (Request $req) : User
    {

        if (self::usernameExists($req->username) && self::emailExists($req->email)) {
            $msg = "User already exists.";
            throw new RecordConflictException($msg);
        }

        $user = new User;
        $user->username = $req->username;
        $user->first_name = $req->first_name;
        $user->last_name = $req->last_name;
        $user->email = $req->email;
        $user->access_level_id = $req->access_level_id;
        $user->password = Hash::make($req->password);
        $user->save();


        if (self::arrayHasItems($req->entity_activity_accesses)) {


            $user->entityActivityAccesses()->attach($req->entity_activity_accesses);

        }

        return $user;

    }



    /**
     * Get users
     * @param $req : Instance of the current request
     * @return User : a collection of user
     */
    public static function getUsers (Request $req) : Paginator
    {

        $users = User::select();

        if ($req->search_query) {

			$searchQuery = $req->search_query;

			$user->where(function($query) use($searchQuery) {

				$names = explode(' ', $searchQuery);

				foreach ($names as $name) {
					$query->where('username', 'LIKE', "%%{$name}%%");
					$query->orWhere('first_name', 'LIKE', "%%{$name}%%");
					$query->orWhere('last_name', 'LIKE', "%%{$name}%%");
				}

			});
		}

        $perPage = $req->per_page? $req->per_page : 20;

        return $users->paginate($perPage);

    }


    /**
     * Get user
     * @param $req : Instance of the current request
     * @param $id : ID of the user
     * @return User : a found user
     */
    public static function getUser (Request $req, $id) : User
    {

        $user = User::select()
                    ->where('id', $id)
                    ->firstOrFail();

        return $user;

    }


    /**
     * Update user
     * @param $req : Instance of the current request
     * @param $id : ID of the user
     * @return Platform : updated user
     */
    public static function updateUser (Request $req, $id) : User
    {

        if (self::usernameExistsExceptForId($id, $req->username)
                && self::emailExistsExceptForId($id, $req->email)) {

            $msg = "User already exists.";
            throw new RecordConflictException($msg);
        }

        $user = User::select()
                    ->where('id', $id)
                    ->firstOrFail();

        $user->username = $req->username;
        $user->first_name = $req->first_name;
        $user->last_name = $req->last_name;
        $user->email = $req->email;
        $user->access_level_id = $req->access_level_id;

        if ($req->password) {
            $user->password = Hash::make($req->password);
        }

        $user->save();

        if (self::arrayHasItems($req->entity_activity_accesses)) {

            $user->entityActivityAccesses()->detach();
            $user->entityActivityAccesses()->sync($req->entity_activity_accesses);

        }

        return $user;
    }

    /**
     * Check if user exists
     * @param $username : username to be checked
     * @return Bool
     */
    protected static function usernameExists ($username) : bool
    {

        $count = User::where('username', $username)->count();

        return $count > 0;

    }


    /**
     * Check if user exists where ID is not equal to the given $id
     * @param $id : ID of the user
     * @param $username : username to be checked
     * @return Bool
     */
    protected static function usernameExistsExceptForId ($id, $username) : bool
    {

        $count = User::where('username', $username)
                    ->where('id', '!=', $id)
                    ->count();

        return $count > 0;

    }


    /**
     * Check if user exists where ID is not equal to the given $id
     * @param $email : email to be checked
     * @return Bool
     */
    protected static function emailExists ($email) : bool
    {

        $count = User::where('email', $email)->count();

        return $count > 0;

    }


    /**
     * Check if user exists where ID is not equal to the given $id
     * @param $id : ID of the user
     * @param $email : email to be checked
     * @return Bool
     */
    protected static function emailExistsExceptForId ($id, $email) : bool
    {

        $count = User::where('username', $username)
                    ->where('id', '!=', $id)
                    ->count();

        return $count > 0;

    }


}




?>
