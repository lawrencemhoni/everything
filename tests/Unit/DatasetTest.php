<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Http\Request;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Dataset;
use App\Models\Field;

use App\RequestModelManagers\DatasetManager;

class DatasetTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * Create Dataset Test
     * @test
     */
    public function user_can_create_dataset () : void
    {
        $data  = [
            'name' => 'Real Estate Data',
            'type_key' => 'entry_dataset_type'
        ];

        $req = Request::create('/create', 'POST', $data);

        $dataset1 = DatasetManager::createDataset($req);

        unset($data['type_key']);

        $this->assertDatabaseHas('datasets', $data);


    }


    /**
     * Create Dataset and Fields Test
     * @test
     */
    public function user_can_create_dataset_and_fields () : void
    {

        $fields = [
            [
                'name' => 'Title',
                'type_key' => 'text_field_type'
            ],
            [
                'name' => 'Title',
                'type_key' => 'text_field_type'
            ]
        ];


        $data  = [
            'name' => 'Real Estate Data',
            'type_key' => 'entry_dataset_type',
            'fields' => $fields
        ];

        $req = Request::create('/create', 'POST', $data);

        $dataset = DatasetManager::createDataset($req);

        unset($data['type_key']);
        unset($data['fields']);

        $this->assertDatabaseHas('datasets', $data);

        foreach ($fields as $field) {
            $check = Dataset::datasetIdHasFieldName($dataset->id,  $field['name']);
            $this->assertTrue($check);
        }
    }


    /**
     * Get Datasets Test
     * @test
     */
    public function user_can_get_datasets () : void
    {
        Dataset::factory()->count(20)->create();

        $req = Request::create('/datasets', 'GET');

        $datasets = DatasetManager::getDatasets($req);

        foreach ($datasets as $dataset) {

            $this->assertDatabaseHas('datasets', [
                'id' => $dataset->id,
                'name' => $dataset->name,
                'type_id' => $dataset->type_id
            ]);

        }
    }



    /**
     * Get Dataset Test
     * @test
     */
    public function user_can_get_dataset () : void
    {
        $dataset = Dataset::factory()->create();

        $req = Request::create('/datasets', 'GET');

        $foundDataset = DatasetManager::getDataset($req, $dataset->id);

        if ($foundDataset->id == $dataset->id && $foundDataset->name == $dataset->name ) {
            $this->assertTrue(true);
        }

    }

    /**
     * Update Dataset Test
     * @test
     */
    public function user_can_update_dataset () : void
    {
        $oldData = [
            'name' => 'Motor Cycles'
        ];

        $oldDataset = Dataset::factory()->create($oldData);

        $this->assertDatabaseHas('datasets', $oldData);

        $newData  = [
            'name' => 'Vehicles Data',
            'type_key' => 'entry_dataset_type'
        ];

        $req = Request::create('/update', 'POST', $newData);

        $foundDataset = DatasetManager::updateDataset($req, $oldDataset->id);

        unset($newData['type_key']);

        $this->assertDatabaseHas('datasets', $newData);

        $this->assertDatabaseMissing('datasets', $oldData);
    }




    /**
     * Update Dataset with Fields Test
     * @test
     */
    public function user_can_update_dataset_with_fields () : void
    {

        $oldData = [
            'name' => 'Motor Cycles',
        ];

        $oldDataset = Dataset::factory()->create($oldData);

        $this->assertDatabaseHas('datasets', $oldData);


        $fields = [
            [
                'name' => 'Title',
                'type_key' => 'text_field_type'
            ],
            [
                'name' => 'Title',
                'type_key' => 'text_field_type'
            ]
        ];

        $newData  = [
            'name' => 'Vehicles Data',
            'type_key' => 'entry_dataset_type',
            'fields' => $fields
        ];

        $req = Request::create('/update', 'POST', $newData);

        $foundDataset = DatasetManager::updateDataset($req, $oldDataset->id);

        unset($newData['type_key']);
        unset($newData['fields']);

        $this->assertDatabaseHas('datasets', $newData);

        $this->assertDatabaseMissing('datasets', $oldData);

        foreach ($fields as $field) {
            $check = Dataset::datasetIdHasFieldName($foundDataset->id,  $field['name']);
            $this->assertTrue($check);
        }

    }









}
