<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntityDataset extends Model
{
    use HasFactory;

    function fields () {
        return $this->belongsToMany(Field::class, 'entity_dataset_field', 'entity_dataset_id', 'field_id');
    }


    public  function scopeDatasetIdHasFieldName($query, $datasetId, $fieldName) {

        $count = $query->select()
                       ->join('entity_dataset_field',
                              'entity_dataset_field.entity_dataset_id',
                              'entity_datasets.id')
                       ->join('fields', 'fields.id', 'entity_dataset_field.field_id')
                       ->where('entity_dataset_field.entity_dataset_id', $datasetId)
                       ->where('fields.name', $fieldName)
                       ->count();

        return $count > 0;

    }




}
