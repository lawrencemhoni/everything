<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;

use App\Models\Type;
use App\Models\Category;
use App\RequestModelManagers\TypeManager;


class TypeTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * Create Type Test
     * @test
     */
    public function user_can_create_type_with_category_key () : void
    {
        $categoryKey = 'entry_types_category';

        $data = [
            'name' => 'News Entry',
            'key' => 'new_entry_type',
            'category_key' => $categoryKey
        ];

        $req = Request::create('/create', 'POST', $data);

        $createdType = TypeManager::createType($req);

        unset($data['category_key']);

        $this->assertDatabaseHas('types', $data);


    }

    /**
     * Create Type Test
     * @test
     */
    public function user_can_create_type_with_category_id () : void
    {

        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        $data = [
            'name' => 'News Entry',
            'key' => 'new_entry_type',
            'category_id' => $category->id
        ];

        $req = Request::create('/create', 'POST', $data);

        $createdType = TypeManager::createType($req);


        $this->assertDatabaseHas('types', $data);

    }


    /**
     * Get Types With Category Key
     * @test
     */
    public  function user_can_get_types_with_category_key () : void
    {
        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        Type::factory()
            ->count(10)
            ->create([
                'category_id' => $category->id
        ]);


        $data = [
            'category_key' => $categoryKey
        ];


        $req = Request::create('/apps', 'GET', $data);

        $types = TypeManager::getTypes($req);

        foreach ($types as $type) {

            $this->assertDatabaseHas('types', [
                'name' => $type->name,
                'key' => $type->key,
                'category_id' => $type->category_id
            ]);
        }

    }

    /**
     * Get Types With Category Key
     * @test
     */
    public  function user_can_get_types_with_category_id () : void
    {
        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        Type::factory()
            ->count(10)
            ->create([
                'category_id' => $category->id
        ]);


        $data = [
            'category_id' =>  $category->id
        ];


        $req = Request::create('/apps', 'GET', $data);

        $types = TypeManager::getTypes($req);

        foreach ($types as $type) {

            $this->assertDatabaseHas('types', [
                'name' => $type->name,
                'key' => $type->key,
                'category_id' => $type->category_id
            ]);
        }

    }

    /**
     * Get Type Test
     * @test
     */
    public function user_can_get_type () : void
    {

        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        $type = Type::factory()
                    ->create([
                        'category_id' => $category->id
                    ]);


        $req = Request::create('/app', 'GET');

        $foundType = TypeManager::getType($req, $type->id);

        if ($type->id == $foundType->id &&
            $type->name == $foundType->name &&
            $type->key == $foundType->key &&
            $type->category_id == $foundType->category_id) {

                $this->assertTrue(true);

        } else {
            $this->assertTrue(false);
        }

    }


    /**
     * Update Type With Category Key Test
     * @test
     */
    public function user_can_update_type_with_category_key () : void
    {

        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        $type = Type::factory()
                    ->create([
                        'category_id' => $category->id
                    ]);

        $data = [
            'name' => 'Imported Products',
            'key' => 'imported_products',
            'category_key' => $categoryKey
        ];

        $req = Request::create('/app', 'POST', $data);

        $updatedType = TypeManager::updateType($req, $type->id);

        unset($data['category_key']);

        $data['id'] = $type->id;
        $data['category_id'] = $type->category_id;

        $this->assertDatabaseHas('types', $data);

        if ($type->id == $updatedType->id &&
            $type->name == $updatedType->name &&
            $type->key == $updatedType->key &&
            $type->category_id == $updatedType->category_id) {

                $this->assertTrue(true);

        } else {
            $this->assertTrue(true);
        }

    }




    /**
     * Update Type With Category ID Test
     * @test
     */
    public function user_can_update_type_with_category_id () : void
    {

        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        $type = Type::factory()
                    ->create([
                        'category_id' => $category->id
                    ]);

        $data = [
            'name' => 'Imported Products',
            'key' => 'imported_products',
            'category_id' => $category->id
        ];

        $req = Request::create('/app', 'POST', $data);

        $updatedType = TypeManager::updateType($req, $type->id);

        $data['id'] = $type->id;
        $data['category_id'] = $type->category_id;

        $this->assertDatabaseHas('types', $data);

        if ($type->id == $updatedType->id &&
            $type->name == $updatedType->name &&
            $type->key == $updatedType->key &&
            $type->category_id == $updatedType->category_id) {

                $this->assertTrue(true);

        } else {
            $this->assertTrue(true);
        }

    }







}
