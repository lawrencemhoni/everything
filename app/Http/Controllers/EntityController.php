<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\ForbiddenAccessException;

use App\RequestModelManagers\EntityDatasetManager;

use App\Services\EntityAccessService;

use Exception;



class EntityController extends Controller
{
    public function __construct (EntityAccessService $entityAccessServices) {
     $this->entityAccess = $entityAccessServices;
    }

    public function createEntityDataset (Request $req, $entityId) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('entity_dataset',
            'create_activity_type');

            $entityDataset = EntityDatasetManager::createEntityDataset($req, $entityId);

            return response()->json(['data' => $entityDataset], 201);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception $e ) {
            \Log::info($e->getMessage());
            return response()->json([], 500);
        }

    }



    public function getEntityDatasets (Request $req, $entityId) {

        try {

            $this->entityAccess->check('entity_dataset',
            'list_activity_type');

            return EntityDatasetManager::getEntityDatasets($req, $entityId);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception $e ) {
            return response()->json([], 500);
        }

    }


    public function viewEntityDataset (Request $req, $entityId, $id) {

        try {

            $this->entityAccess->check('entity_dataset',
            'view_activity_type');

            $entityDataset = EntityDatasetManager::getEntityDataset($req, $entityId, $id);

            return response()->json(['data' => $entityDataset], 200);


        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception  $e) {
            return response()->json([], 500);
        }
    }



    public function updateEntityDataset (Request $req, $entityId, $id) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('entity_dataset',
            'update_activity_type');

            $entityDataset =  EntityDatasetManager::updateEntityDataset($req, $entityId, $id);

            return response()->json(['data' => $entityDataset], 200);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        }  catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException $e) {
            return response()->json([], 404);
        } catch ( Exception $e) {
            \Log::info($e->getMessage());
            return response()->json([], 500);
        }

    }






}
