<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Type;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\App>
 */
class AppFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $typeIds = \App\Models\Type::select('types.id')
                                ->filterByCategoryKey('app_types_category')
                                ->get()
                                ->pluck('id')
                                ->toArray();

        $rand = array_rand($typeIds, 1);
        $key = is_array($rand)? array_shift($rand) : $rand ;

        return [
            'name' => fake()->name(),
            'type_id' => $typeIds[$key],

        ];
    }
}
