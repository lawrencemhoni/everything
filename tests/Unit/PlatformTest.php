<?php

namespace Tests\Unit;

use Tests\TestCase;


use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;

use App\Models\Platform;
use App\RequestModelManagers\PlatformManager;


class PlatformTest extends TestCase
{
    use DatabaseMigrations;


    /**
     * Create platform
     * @test
     */
    public function user_can_create_platform () {

        $data = [
            'name' => 'Electronics'
        ];

        $req = Request::create('/create', 'POST', $data);

        $user = PlatformManager::createPlatform($req);

        $this->assertDatabaseHas('platforms', $data);
    }



    /**
     * Get platforms
     * @test
     */
    public function user_can_get_platforms () {

        $platforms = Platform::factory()->count(10)->create();

        $req = Request::create('/user', 'GET');

        $foundPlatforms = PlatformManager::getPlatforms($req);

        foreach ($foundPlatforms as $platform) {
            $this->assertDatabaseHas('platforms', [
                'name' => $platform->name
            ]);
        }

    }




    /**
     * Get platform
     * @test
     */
    public function user_can_get_platform () {

        $platform = Platform::factory()->create();

        $req = Request::create('/user', 'GET');

        $foundPlatform = PlatformManager::getPlatform($req, $platform->id);

        if ($foundPlatform->name == $platform->name) {
            $this->assertTrue(true);
        }

    }

    /**
     * Update platform
     * @test
     */
    public function user_can_update_platform () : void
    {
        $oldData = [
            'name' => 'Baby Stuff'
        ];

        $oldPlatform = Platform::factory($oldData)->create();

        $this->assertDatabaseHas('platforms', $oldData);


        $newData = [
            'name' => 'Toddler fashion'
        ];

        $req = Request::create('/user', 'POST', $newData);

        $foundPlatform = PlatformManager::updatePlatform($req, $oldPlatform->id);

        $this->assertDatabaseHas('platforms', $newData);

        $this->assertDatabaseMissing('platforms', $oldData);

    }






}
