<?php

namespace App\RequestModelManagers;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Illuminate\Http\Request;


use App\Models\Platform;
use App\Exceptions\RecordConflictException;
use App\Exceptions\InvalidDataException;


class PlatformManager extends RequestModelManager {



    /**
     * Create platform
     * @param $req : Instance of the current request
     * @return Platform : created platform
     */
    public static function createPlatform (Request $req) : Platform
    {

        if (self::platformExists($req->name)) {
            $msg = "Platform ({$req->name}) already exists.";
            throw new RecordConflictException($msg);
        }

        $platform = new Platform;
        $platform->name = $req->name;
        $platform->save();

        if ($req->apps && count($req->apps)) {
            $platform->apps()->attach($req->apps);
        }

        return self::getPlatformWithRelationshipsByIdOrFail($platform->id);
    }


    /**
     * Get platforms
     * @param $req : Instance of the current request
     * @return Platform : a collection of platforms
     */
    public static function getPlatforms (Request $req) : Paginator
    {
        $platforms = Platform::select();

        if ($req->search_query) {
            $platforms->where('name', 'LIKE', "%%{$req->search_query}%%");
        }

        return $platforms->paginate();
    }


    /**
     * Get platform
     * @param $req : Instance of the current request
     * @param $id : ID of the platform
     * @return Platform : a found platform
     */
    public static function getPlatform (Request $req, $id) : Platform
    {
        return self::getPlatformWithRelationshipsByIdOrFail($id);
    }


    /**
     * Update platform
     * @param $req : Instance of the current request
     * @param $id : ID of the platform
     * @return Platform : updated platform
     */
    public static function updatePlatform (Request $req, $id) : Platform
    {
        if (self::platformExistsExceptForId($id, $req->name)) {
            $msg = "Platform ({$req->name}) already exists.";
            throw new RecordConflictException($msg);
        }

        $platform = self::getPlatformByIdOrFail($id);

        $platform->name = $req->name;
        $platform->save();

        if ($req->apps && count($req->apps)) {
            $platform->apps()->detach();
            $platform->apps()->attach($req->apps);
        }

        $platform = self::getPlatformWithRelationshipsByIdOrFail($id);

        return $platform;
    }


    /**
     * Add app platform
     * @param $req : Instance of the current request
     * @param $id : ID of the platform
     * @return Platform : the platform
     */
    public static function addPlatformApp (Request $req, $id) : Platform
    {

        $platform = self::getPlatformByIdOrFail($id);

        $platform->apps()->sync($req->app_id, false);

        return $platform;
    }


    /**
     * Get platform by ID or throw an exception
     * @param $id : ID of the platform
     * @return Platform : a found dataset
     */
    protected static function getPlatformByIdOrFail ($id) : Platform
    {
        $platform = Platform::select()
                        ->where('id', $id)
                        ->firstOrFail();

        return $platform;
    }


     /**
     * Get platform with relationships by ID or throw an exception
     * @param $id : ID of the platform
     * @return Platform : a found dataset
     */
    protected static function getPlatformWithRelationshipsByIdOrFail ($id) : Platform
    {
        $platform = Platform::select()
                        ->where('id', $id)
                        ->with('apps')
                        ->firstOrFail();

        return $platform;
    }


    /**
     * Check if platform exists
     * @param $name : name of the platform
     * @return Bool
     */
    protected static function platformExists ($name)
    {
        $count = Platform::where('name', $name)
                         ->count();

        return $count > 0;
    }


    /**
     * Check if platform exists where ID is not equal to the given $id
     * @param $id : ID of the platform
     * @param $name : name of the platform
     * @return Bool
     */
    protected static function platformExistsExceptForId ($id, $name)
    {
        $count = Platform::where('id', '!=', $id)
                         ->where('name', $name)
                         ->count();

        return $count > 0;
    }

}
