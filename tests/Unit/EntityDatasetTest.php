<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Http\Request;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Entity;
use App\Models\EntityDataset;
use App\Models\Field;

use App\RequestModelManagers\EntityDatasetManager;

class EntityDatasetTest extends TestCase
{


    /**
     * Create EntityDataset Test
     * @test
     */
    public function user_can_create_entity_dataset () : void
    {
        $entity = Entity::selectBasics()
                        ->filterByTypeKey('public_entity_type')
                        ->first();

        $data  = [
            'name' => 'Real Estate Data',
        ];

        $req = Request::create('/create', 'POST', $data);

        $dataset = EntityDatasetManager::createEntityDataset($req, $entity->id);

        $data['entity_id'] = $entity->id;

        $this->assertDatabaseHas('entity_datasets', $data);

    }


    /**
     * Create EntityDataset and Fields Test
     * @test
     */
    public function user_can_create_entity_dataset_and_fields () : void
    {

        $entity = Entity::selectBasics()
                        ->filterByTypeKey('public_entity_type')
                        ->first();

        $fields = [
            [
                'name' => 'Title',
                'type_key' => 'text_field_type'
            ],
            [
                'name' => 'Description',
                'type_key' => 'long_text_field_type'
            ]
        ];


        $data  = [
            'name' => 'Real Estate Data',
            'fields' => $fields
        ];

        $req = Request::create('/create', 'POST', $data);

        $entityDataset = EntityDatasetManager::createEntityDataset($req, $entity->id);

        unset($data['fields']);

        $this->assertDatabaseHas('entity_datasets', $data);

        foreach ($fields as $field) {
            $check = EntityDataset::datasetIdHasFieldName($entityDataset->id,  $field['name']);
            $this->assertTrue($check);
        }
    }


    /**
     * Get EntityDatasets Test
     * @test
     */
    public function user_can_get_entity_datasets () : void
    {

        $entity = Entity::selectBasics()
                        ->filterByTypeKey('public_entity_type')
                        ->first();

        EntityDataset::factory()->count(20)->create(['entity_id' => $entity->id]);

        $req = Request::create('/datasets', 'GET');

        $entityDatasets = EntityDatasetManager::getEntityDatasets($req, $entity->id);

        foreach ($entityDatasets as $dataset) {

            $this->assertDatabaseHas('entity_datasets', [
                'id' => $dataset->id,
                'name' => $dataset->name,
                'entity_id' => $entity->id
            ]);

        }
    }



    /**
     * Get EntityDataset Test
     * @test
     */
    public function user_can_get_entity_dataset () : void
    {
        $entityDataset = EntityDataset::factory()->create();

        $req = Request::create('/datasets', 'GET');

        $foundEntityDataset = EntityDatasetManager::getEntityDataset(
            $req, $entityDataset->entity_id, $entityDataset->id);

        if ($foundEntityDataset->id == $entityDataset->id &&
            $foundEntityDataset->name == $entityDataset->name &&
            $foundEntityDataset->entity_id == $entityDataset->entity_id) {

            $this->assertTrue(true);
        }

    }

    /**
     * Update EntityDataset Test
     * @test
     */
    public function user_can_update_entity_dataset () : void
    {
        $entityDataset = EntityDataset::factory()->create();

        $newData  = [
            'name' => 'Vehicles Data',
        ];

        $req = Request::create('/update', 'POST', $newData);

        $updatedEntityDataset = EntityDatasetManager::updateEntityDataset(
                                                     $req, $entityDataset->entity_id,
                                                     $entityDataset->id);

        $this->assertDatabaseHas('entity_datasets', [
                'id' => $entityDataset->id,
                'name' => $newData['name'],
                'entity_id' => $entityDataset->entity_id
        ]);

        $this->assertDatabaseMissing('entity_datasets', [
            'id' => $entityDataset->id,
            'name' => $entityDataset->name,
            'entity_id' => $entityDataset->entity_id
        ]);
    }


    /**
     * Update EntityDataset Test
     * @test
     */
    public function user_can_update_entity_dataset_with_fields () : void
    {
        $entityDataset = EntityDataset::factory()->create();

        $fields = [
            [
                'name' => 'Title',
                'type_key' => 'text_field_type'
            ],
            [
                'name' => 'Decription',
                'type_key' => 'long_text_field_type'
            ]
        ];

        $newData  = [
            'name' => 'Vehicles Data',
            'fields' => $fields
        ];

        $req = Request::create('/update', 'POST', $newData);

        $updatedEntityDataset = EntityDatasetManager::updateEntityDataset(
                                                      $req, $entityDataset->entity_id,
                                                      $entityDataset->id);

        foreach ($fields as $field) {
            $check = EntityDataset::datasetIdHasFieldName($updatedEntityDataset->id,  $field['name']);
            $this->assertTrue($check);
        }


        $this->assertDatabaseHas('entity_datasets', [
                'id' => $entityDataset->id,
                'name' => $newData['name'],
                'entity_id' => $entityDataset->entity_id
        ]);

        $this->assertDatabaseMissing('entity_datasets', [
            'id' => $entityDataset->id,
            'name' => $entityDataset->name,
            'entity_id' => $entityDataset->entity_id
        ]);
    }







}
