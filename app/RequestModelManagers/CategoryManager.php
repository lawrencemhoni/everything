<?php

namespace App\RequestModelManagers;


use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Illuminate\Http\Request;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;


use App\Models\Category;
use App\Models\Type;

use DB;


class CategoryManager extends RequestModelManager {


    protected static $authorizedTypeKeys = [
        'entry_category_type',
        'entry_location_category_type'
    ];

    /**
     * Create category
     * @param $id : id of the category
     * @return Category
     */
    public static function createCategory (Request $req) : Category
    {

        $type = self::getValidTypeByIdOrKeyOrFail($req->type_id, $req->type_key);

        if (self::categoryKeyExists ($req->key)) {
            $msg = "Category key {$req->key} exists";
            throw new RecordConflictException($msg);
        }

        $category = new Category;
        $category->name = $req->name;
        $category->key = $req->key;
        $category->type_id = $type->id;

        if ($req->parent_id) {
            $category->parent_id = $req->parent_id;
        }

        $category->save();

        return $category;

    }

    /**
     * Update category
     * @param $id : id of the category
     * @return Category
     */
    public static function updateCategory (Request $req, $id) : Category
    {

        $type = self::getValidTypeByIdOrKeyOrFail($req->type_id, $req->type_key);

        if (self::categoryKeyExistsExceptForId($id, $req->key)) {
            $msg = "category key {$req->key} exists";
            throw new RecordConflictException($msg);
        }

        $category = Category::select()
                            ->where('id', $id)
                            ->firstOrFail();

        $category->name = $req->name;
        $category->type_id = $type->id;
        $category->key = $req->key;

        if ($req->parent_id) {
            $category->parent_id = $req->parent_id;
        }

        $category->save();

        return $category;

    }

    /**
     * Get a collection of categories
     * @return Category
     */
    public static function  getCategories (Request $req) : Paginator
    {

        $type = self::getValidTypeByIdOrKeyOrFail($req->type_id, $req->type_key);

        $categories = Category::select()
                            ->where('type_id', $type->id)
                            ->with('childCategories')
                            ->paginate();

        return $categories;

    }

    /**
     * Find category
     * @param $id : id of the category
     * @return Category
     */
    public static function  getCategory (Request $req, $id) : Category
    {

        $categories = Category::select(DB::raw('DISTINCT categories.id'),
                                       'categories.name', 'categories.key',
                                       'categories.type_id')
                                ->join('types', 'types.id', 'categories.type_id')
                                ->where('categories.id', $id)
                                ->whereIn('types.key', self::$authorizedTypeKeys)
                                ->firstOrFail();

        return $categories;

    }

    /**
     * Check if category key exists
     * @param $key : key of the type
     * @return Bool
     */

    public static function categoryKeyExists ($key) : bool
    {
       return  Category::where('key', $key)->count() > 0;
    }



    /**
     * Check if category key exists except where there is a specific id
     * @param $key : key of the type
     * @return Bool
     */

    public static function categoryKeyExistsExceptForId ($id, $key) : bool
    {
        $count  =  Category::where('key', $key)
                        ->where('id', '!=', $id)
                        ->count();
        return $count > 0;
    }



    /**
     * Get valid type with the given key
     * @param $key : key of the type
     * @return Type : a found type
     */
    public static function getValidTypeByKey($key)
    {
        if (!in_array($key, self::$authorizedTypeKeys)) {
            return null;
        }

       return Type::where('key', $key)->first();

    }


    /**
     * Get valid type with the given key or throw an exception
     * @param $key : key of the type
     * @return Type : a found type
     */
    public static function getValidTypeByKeyOrFail($key)
    {
       $type = self::getValidTypeByKey($key);

       if (!$type) {

        $msg = "Invalid type key: {$key}";
        throw new InvalidDataException($msg);

       }

       return $type;

    }



    /**
     * Get valid type with the given ID
     * @param $key : key of the type
     * @return Type : a found type
     */
    public static function getValidTypeById($id)
    {

        $type = Type::where('id', $id)
                    ->whereIn('key', self::$authorizedTypeKeys)
                    ->first();

        return $type;

    }


    /**
     * Get valid type with the given ID
     * @param $key : key of the type
     * @return Type : a found type
     */
    public static function getValidTypeByIdOrFail($id)
    {

        $type = self::getValidTypeById($id);

       if (!$type) {

            $msg = "Invalid type id: {$id}";
            throw new InvalidDataException($msg);

        }

       return $type;

    }


    /**
     * Get valid type with the given key or throw an exception
     * @param $key : key of the type
     * @return Type : a found type
     */
    public static function getValidTypeByIdOrKeyOrFail($id, $key) : Type
    {
        $type;

        if ($id) {

            $type = self::getValidTypeById($id);

        } else if ($key) {

            $type = self::getValidTypeByKey($key);

        }


       if (!$type) {

            $msg = "Invalid type key: {$key}";
            throw new InvalidDataException($msg);

       }

       return $type;

    }




}
