<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Testing\Fluent\AssertableJson;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Category;
use App\Models\Type;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;

    /**
     *Create Category with Type Key Test
     *@test
     */
    public function user_can_create_category_with_type_key () : void
    {

        $type = Type::select()
                    ->where('key', 'entry_category_type')
                    ->first();

        $url = 'platform-zero/categories/create';

        $data = [
            'name' => 'Lady clothes',
            'key' => 'lady_clothes',
            'type_key' => 'entry_category_type'
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertStatus(201)
                 ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                         ->where('data.key', $data['key'])
                         ->where('data.type_id', $type->id)
                         ->etc()
                );
    }



    /**
     *Create Category Test with Type ID
     *@test
     */
    public function user_can_create_category_with_type_id () : void
    {

        $type = Type::select()
                    ->where('key', 'entry_category_type')
                    ->first();

        $url = 'platform-zero/categories/create';

        $data = [
            'name' => 'Baby clothes',
            'key' => 'baby_clothes',
            'type_id' => $type->id
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertStatus(201)
                 ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                         ->where('data.key', $data['key'])
                         ->where('data.type_id', $type->id)
                         ->etc()
                );

    }



   /**
     * Get Categories Test
     * @test
     */
    public function user_can_get_categories () : void
    {

        $type = Type::where('key', 'entry_category_type')->first();

        $categories = Category::factory()->count(20)->create([
            'type_id' => $type->id
        ]);


        #dataparams wasn't working so I didn't mind hacking it.
        $url = '/platform-zero/categories?type_key=entry_category_type';

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ]);
    }




    /**
     * Get category test
     * @test
     */
    public function user_can_get_category () : void
    {
        $type = Type::where('key', 'entry_category_type')->first();

        $category = Category::factory()->create([
            'type_id' => $type->id
        ]);

        $url = "/platform-zero/categories/{$category->id}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('data.id', $category->id)
                 ->where('data.name', $category->name)
                 ->etc()
        );

    }


    /**
     *Update with Type Key Test
     *@test
     */
    public function user_can_update_category_with_type_key () : void
    {
        $type = Type::where('key', 'entry_category_type')->first();

        $category = Category::factory()->create([
            'type_id' => $type->id
        ]);

        $url = "/platform-zero/categories/{$category->id}/update";

        $data = [
            'name' => 'Kitchen Stuff',
            'key' => 'kitchen_stuff',
            'type_key' => 'entry_category_type'
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.name', $data['name'])
                    ->etc()
        );

    }


    /**
     *Update Category with Type Key Test
     *@test
     */
    public function user_can_update_category_with_type_id () : void
    {
        $type = Type::where('key', 'entry_category_type')->first();

        $category = Category::factory()->create([
            'type_id' => $type->id
        ]);

        $url = "/platform-zero/categories/{$category->id}/update";

        $data = [
            'name' => 'Kitchen Stuff',
            'key' => 'kitchen_stuff',
            'type_id' => $type->id
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.name', $data['name'])
                    ->etc()
        );

    }





}
