<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Category;
use App\Models\Type;


class TypeSeeder extends Seeder
{
    public $types = [

        [
            'name' => 'Private Entity',
            'key' => 'private_entity_type',
            'category_key' => 'entity_types_category'
        ],

        [
            'name' => 'Public Entity',
            'key' => 'public_entity_type',
            'category_key' => 'entity_types_category'
        ],

        [
            'name' => 'Protected Entity',
            'key' => 'protected_entity_type',
            'category_key' => 'entity_types_category'
        ],

        [
            'name' => 'Abtract Entity',
            'key' => 'abstract_entity_type',
            'category_key' => 'entity_types_category'
        ],

        [
            'name' => 'Type Category',
            'key' => 'type_category_type',
            'category_key' => 'category_types_category'
        ],

        [
            'name' => 'Entry Category',
            'key' => 'entry_category_type',
            'category_key' => 'category_types_category',
            'context_identifer' => 'Categories'
        ],

        [
            'name' => 'Entry Location Category',
            'key' => 'entry_location_category_type',
            'category_key' => 'category_types_category',
            'context_identifer' => 'Locations'
        ],


        [
            'name' => 'SSR Web App',
            'key' => 'ssr_web_app_type',
            'category_key' => 'app_types_category'
        ],

        [
            'name' => 'CSR Web App',
            'key' => 'csr_web_app_type',
            'category_key' => 'app_types_category'
        ],

        [
            'name' => 'Android Mobile App',
            'key' => 'android_mobile_app_type',
            'category_key' => 'app_types_category'
        ],

        [
            'name' => 'IOS Mobile App',
            'key' => 'ios_mobile_app_type',
            'category_key' => 'app_types_category'
        ],


        [
            'name' => 'Entry Dataset',
            'key' => 'entry_dataset_type',
            'category_key' => 'dataset_types_category'
        ],

        [
            'name' => 'Category Dataset',
            'key' => 'category_dataset_type',
            'category_key' => 'dataset_types_category'
        ],

        [
            'name' => 'Ad Entry',
            'key' => 'ad_entry_type',
            'category_key' => 'entry_types_category'
        ],

        [
            'name' => 'Super Access Level',
            'key' => 'super_access_level_type',
            'category_key' => 'access_level_types_category'
        ],

        [
            'name' => 'Admin Access Level',
            'key' => 'admin_access_level_type',
            'category_key' => 'access_level_types_category'
        ],

        [
            'name' => 'Admin Access Level',
            'key' => 'public_access_level_type',
            'category_key' => 'access_level_types_category'
        ],


        [
            'name' => 'Login',
            'key' => 'login_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Logout',
            'key' => 'logout_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Create',
            'key' => 'create_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'List',
            'key' => 'list_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'View',
            'key' => 'view_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Update',
            'key' => 'update_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Delete',
            'key' => 'delete_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Search',
            'key' => 'search_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Upload',
            'key' => 'upload_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Copy',
            'key' => 'copy_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Move',
            'key' => 'move_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Download',
            'key' => 'download_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Authorize',
            'key' => 'authorize_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Unauthorize',
            'key' => 'authorize_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Grant',
            'key' => 'grant_activity_type',
            'category_key' => 'activity_types_category'
        ],

        [
            'name' => 'Revoke',
            'key' => 'revoke_activity_type',
            'category_key' => 'activity_types_category'
        ],


        [
            'name' => 'Directory',
            'key' => 'directory_file_type',
            'category_key' => 'file_types_category'
        ],


        [
            'name' => 'Document',
            'key' => 'document_file_type',
            'category_key' => 'file_types_category'
        ],

        [
            'name' => 'Image',
            'key' => 'image_file_type',
            'category_key' => 'file_types_category'
        ],

        [
            'name' => 'Video',
            'key' => 'video_file_type',
            'category_key' => 'file_types_category'

        ],

        [
            'name' => 'Audio',
            'key' => 'audio_file_type',
            'category_key' => 'file_types_category'
        ],

        [
            'name' => 'Text',
            'key' => 'text_field_type',
            'category_key' => 'field_types_category'
        ],

        [
            'name' => 'Long Text',
            'key' => 'long_text_field_type',
            'category_key' => 'field_types_category'
        ],

        [
            'name' => 'Select',
            'key' => 'select_field_type',
            'category_key' => 'field_types_category'
        ],

        [
            'name' => 'Check Options',
            'key' => 'check_options_field_type',
            'category_key' => 'field_types_category'
        ],


    ];


    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        foreach ($this->types as $item) {

            $category = Category::where('key', $item['category_key'])
                                ->firstOrFail();

            $type = Type::firstOrCreate(
                ['key' => $item['key']],
                [
                    'key' => $item['key'],
                    'name' => $item['name'],
                    'context_identifier' => isset($item['context_identifier'])? $item['context_identifier'] : null,
                    'category_id' => $category->id
                ]
            );

        }


        $categoryType = Type::select()
                            ->where('key', 'type_category_type' )
                            ->first();

        $typeCategories = Category::where('type_id', 1 )
                                  ->update([
                                    'type_id' =>  $categoryType->id
                                  ]);

    }
}
