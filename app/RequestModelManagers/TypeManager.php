<?php

namespace App\RequestModelManagers;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Illuminate\Http\Request;

use App\Models\Type;
use App\Models\Category;

use App\Models\Platform;
use App\Exceptions\RecordConflictException;
use App\Exceptions\InvalidDataException;

use DB;


class TypeManager extends RequestModelManager {

    private static $authorizedCategoryKeys = [
        'entry_types_category',
        'dataset_types_category',
        'app_types_category'
    ];




    /**
     * Create type
     * @return Type
     */
    public static function createType (Request $req) : Type
    {
        $category = self::getValidCategoryByIdOrKeyOrFail($req->category_id, $req->category_key);

        if (self::typeKeyExists($req->key)) {
            $msg = "Type key {$req->key} exists";
            throw new RecordConflictException($msg);
        }

        $type = new Type;
        $type->name = $req->name;
        $type->key = $req->key;
        $type->category_id = $category->id;
        $type->context_identifier = $req->context_identifier;

        $type->save();

        return $type;

    }

    /**
     * Get Types
     * @return Paginator : A paginated collection of types
     */
    public static function getTypes (Request $req) : Paginator
    {

        $category = self::getValidCategoryByIdOrKeyOrFail($req->category_id, $req->category_key);

        $types = Type::select()
                    ->where('category_id', $category->id)
                    ->paginate();

        return $types;
    }

    /**
     * Find type
     * @param $id : id of the type
     * @return Type
     */
    public static function getType (Request $req, $id) : Type
    {

        $type = Type::select(DB::raw('DISTINCT types.id'),
                            'types.name', 'types.category_id', 'types.key')
                    ->join('categories', 'categories.id', 'types.category_id')
                    ->whereIn('categories.key', self::$authorizedCategoryKeys)
                    ->where('types.id', $id)
                    ->firstOrFail();

        return $type;

    }

    /**
     * Update type
     * @param $id : id of the type
     * @return Category
     */
    public static function updateType (Request $req, $id) : Type
    {

        $category = self::getValidCategoryByIdOrKeyOrFail($req->category_id, $req->category_key);

        if (self::typeKeyExistsExceptForId($id, $req->key)) {
            $msg = "Type key {$req->key} exists";
            throw new RecordConflictException($msg);
        }

        $type = Type::select(DB::raw('DISTINCT types.id'),
                             'types.name', 'types.category_id', 'types.key')
                            ->join('categories', 'categories.id', 'types.category_id')
                            ->whereIn('categories.key', self::$authorizedCategoryKeys)
                            ->where('types.id', $id)
                            ->firstOrFail();

        $type->name = $req->name;
        $type->key = $req->key;
        $type->category_id = $category->id;
        $type->context_identifier = $req->context_identifier;

        $type->save();

        return $type;
    }


    /**
     * Check if type key exists.
     * @param $key : key of the type
     * @return Bool
     */
    public static function typeKeyExists ($key) : bool
    {
        return  Type::where('key', $key)->count() > 0;
    }


    /**
     * Check if type key exists except where there is a specific Id
     * @param $id : Id of the type where we should skip
     * @param $key : key of the type
     * @return Bool
     */
    public static function typeKeyExistsExceptForId ($id, $key) : bool
    {
        $count = Type::where('key', $key)
                    ->where('id', '!=', $id)
                    ->count();

        return $count > 0;
    }


    /**
     * Get Valid Category Id
     * @param $id : Id of the category
     * @return Category
     */
    public static function getValidCategoryById ($id) : Category
    {

        $category = Category::where('id', $id)
                        ->whereIn('key', self::$authorizedCategoryKeys)
                        ->first();

        return $category;

    }


    /**
     * Get Valid Category Id or throw an exception if the category is not found.
     * @param $id : Id of the category
     * @return Category
     */
    public static function getValidCategoryByIdOrFail ($id) : Category
    {

        $category = self::getValidCategoryById($id);

        if (!$category) {

            $msg = "Invalid category id: {$id}";
            throw new InvalidDataException($msg);

        }

        return $category;

    }


    /**
     * Get Valid Category using Key
     * @param $key : key of the category
     * @return Category
     */
    public static function getValidCategoryByKey ($key)
    {
        if (!in_array($key, self::$authorizedCategoryKeys)) {
            return;
        }

        $category = Category::where('key', $key)
                        ->first();

        return $category;

    }



    /**
     * Get Valid Category using Key or throw an exception if not found.
     * @param $key : key of the category
     * @return Category
     */
    public static function getValidCategoryByKeyOrFail ($key) : Category
    {

        $category = self::getValidCategoryByKey($key);

        if (!$category) {

            $msg = "Invalid category key: {$key}";
            throw new InvalidDataException($msg);

        }

        return $category;

    }

    /**
     * Get Valid Category using either ID or Key
     * And throw an exception if there is no result
     * @param $key : key of the category
     * @return Category
     */
    public static function getValidCategoryByIdOrKeyOrFail ($id, $key) : Category
    {

        $category;

        if ($id) {

            $category = self::getValidCategoryById($id);

        } else if ($key) {

            $category = self::getValidCategoryByKey($key);
        }

        if (!$category) {

            $msg = "Invalid category key: {$key}";
            throw new InvalidDataException($msg);

        }

        return $category;

    }







}



?>
