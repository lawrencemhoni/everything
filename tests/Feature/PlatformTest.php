<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


use Illuminate\Testing\Fluent\AssertableJson;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;

use App\Models\Platform;
use App\RequestModelManagers\PlatformManager;



class PlatformTest extends TestCase
{

    use DatabaseMigrations;



    /**
     * Create Platform Test
     * @test
     */
    public function user_can_create_platform () : void
    {


        $url = 'platform-zero/platforms/create';

        $data = [
            'name' => 'Electronics Hub',
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertStatus(201)
                 ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                        ->etc()
                );



    }


    /**
     * Get Platform Test
     * @test
     */
    public function user_can_get_platform() : void
    {

        $platform = Platform::factory()->create();

        $url = "/platform-zero/platforms/{$platform->id}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('data.id', $platform->id)
                 ->where('data.name', $platform->name)
                 ->etc()
        );


    }


    /**
     * Get Users Test
     * @test
     */
    public function user_can_get_platforms () : void
    {

        Platform::factory()->count(20)->create();

        $url = '/platform-zero/platforms';

        $response = $this->actingAsTestUser()->getJson($url);


        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ]);

    }



    /**
     *Update Platform Test
     *@test
     */
    public function user_can_update_platform () : void
    {
        $platform = Platform::factory()->create();

        $url = "/platform-zero/platforms/{$platform->id}/update";

        $data = [
            'name' => 'Preowned Items',
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.name', $data['name'])
                    ->etc()
        );
    }






}
