<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\UnauthorizedAccessException;

use App\Models\User;
use Hash;

class AuthController extends Controller
{
    public function login (Request $req) {

        try {


            $user = User::where(function ($query) use ($req) {

                $query->where('username', $req->username)
                      ->orWhere('email', $req->email);

            })->firstOrFail();

            $passwordsMatch = Hash::check($req->password, $user->password);

            if (!$passwordsMatch) {
                $msg = 'Passwords do not match';
                throw new UnauthorizedAccessException($msg);
            }

            $token = $user->createToken('Auth')->accessToken;

            $data = [
                'token' => $token
            ];

            return response()->json(['data' => $data], 200);


        } catch (ModelNotFoundException $e) {
            return response()->json([], 401);
        }  catch (UnauthorizedAccessException $e) {
            return response()->json([], 401);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }


        return $token;

    }


    public function logout () {
        $authUser = auth()->user();
        $authUser->token()->revoke();

        return response()->json(['message' => 'Logout successful'], 200);
    }

}
