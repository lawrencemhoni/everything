<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Exception;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\ForbiddenAccessException;

use App\RequestModelManagers\PlatformManager;

use App\Services\EntityAccessService;

class PlatformController extends Controller
{

    public function __construct (EntityAccessService $entityAccessServices) {
        $this->entityAccess = $entityAccessServices;
    }


    public function createPlatform (Request $req) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('platform',
            'create_activity_type');

            $platform = PlatformManager::createPlatform($req);

            return response()->json(['data' => $platform], 201);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }

    }

    public function getPlatforms (Request $req) {

        try {

            $this->entityAccess->check('platform',
            'list_activity_type');

            return PlatformManager::getPlatforms($req);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }

    }

    public function viewPlatform (Request $req, $id) {

        try {
            $this->entityAccess->check('platform',
            'view_activity_type');

            $platform = PlatformManager::getPlatform($req, $id);

            return response()->json(['data' => $platform], 200);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException $e ) {
            return response()->json([], 404);
        } catch ( Exception $e) {
            return response()->json([], 500);
        }
    }


    public function updatePlatform (Request $req, $id) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('platform',
            'update_activity_type');

            $platform = PlatformManager::updatePlatform($req, $id);

            return response()->json(['data' => $platform], 200);

        }  catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException $e ) {
            return response()->json([], 404);
        } catch ( Exception $e ) {
            return response()->json([], 500);
        }

    }

}
