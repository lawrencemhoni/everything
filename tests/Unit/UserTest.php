<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


use Illuminate\Pagination\LengthAwarePaginator as Paginator;


use App\RequestModelManagers\UserManager;
use App\Models\User;


use Illuminate\Http\Request;


class UserTest extends TestCase
{
    use DatabaseMigrations;


    public function setUp () : void
    {

        parent::setUp();

    }


    /**
     * Check if user can get users.
     * @test
     */
    public function user_can_get_users () : void
    {

        User::factory()->count(20)->create();

        $req = Request::create('/users', 'GET');

        $users = UserManager::getUsers($req);

        foreach ($users as $user) {
            $this->assertDatabaseHas('users', [
                'username' => $user->username,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email
            ]);
        }

    }


    /**
     * Check if user can create users.
     * @test
     */

     public function user_can_create_user () : void
     {
        $data = [
            'username' => 'JDoe',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'access_level_id' => 1,
            'email' => 'john@email.com',
            'password' => 'abc123',
            'entity_activity_accesses' => [10,11,12,13]
        ];

        $req = Request::create('/create', 'POST', $data);

        $user = UserManager::createUser($req);

        $accesses = $data['entity_activity_accesses'];

        unset($data['entity_activity_accesses']);
        unset($data['password']);

        $this->assertDatabaseHas('users', $data);

        foreach ($accesses as $accessId) {
            $this->assertDatabaseHas('user_entity_activity_access', [
                'user_id' => $user->id,
                'entity_activity_access_id' => $accessId,
            ]);
        }

        $this->assertInstanceOf(User::class, $user);

     }

     /**
      * Check if user can view user
      * @test
      */
    public function user_can_view_user () : void
    {

        $userData = [

            'username' => 'JackDoe',
            'first_name' => 'Jack',
            'last_name' => 'Doe',
            'access_level_id' => 1,
            'email' => 'jack@email.com'
        ];

        $accesses = [10,11,12,13];


        $user = User::factory()->create($userData);

        $user->entityActivityAccesses()->attach($accesses);


        $req = Request::create('/update', 'POST', $userData);

        $newUser = UserManager::getUser($req, $user->id);

        if ($user->username == $newUser->username) {

            $this->assertTrue(true);
        }

    }

    /**
     * Check if user can get users.
     * @test
     */
     public function user_can_update_user () : void
     {

        $oldUserData = [

            'username' => 'JaneDoe',
            'first_name' => 'Jane',
            'last_name' => 'Doe',
            'access_level_id' => 1,
            'email' => 'jane@email.com'
        ];

        $oldAccesses = [10,11,12 ];


        $user = User::factory()->create($oldUserData);

        $user->entityActivityAccesses()->attach($oldAccesses);

        //check if database has old user data

        $this->assertDatabaseHas('users', $oldUserData);

        foreach ($oldAccesses as $accessId) {
            $this->assertDatabaseHas('user_entity_activity_access', [
                'user_id' => $user->id,
                'entity_activity_access_id' => $accessId,
            ]);
        }


        //Let's update with new user data

        $newUserData = $oldUserData;


        $newUserData['username'] = 'JimmyDone';
        $newUserData['first_name'] = 'Jimmy';
        $newUserData['last_name'] = 'Doe';


        $newAccesses = [1,2,3,4,5,6];


        $newUserData['entity_activity_accesses'] = $newAccesses;


        $req = Request::create('/update', 'POST', $newUserData);

        $newUser = UserManager::updateUser($req, $user->id);


        //Let's check if the database has the new user data

        unset($newUserData['entity_activity_accesses']);

        $this->assertDatabaseHas('users', $newUserData);

        foreach ($newAccesses as $accessId) {
            $this->assertDatabaseHas('user_entity_activity_access', [
                'user_id' => $user->id,
                'entity_activity_access_id' => $accessId,
            ]);
        }

       //Let's check again if the database does not have the old user data

        $this->assertDatabaseMissing('users', $oldUserData);

        foreach ($oldAccesses as $accessId) {
            $this->assertDatabaseMissing('user_entity_activity_access', [
                'user_id' => $user->id,
                'entity_activity_access_id' => $accessId,
            ]);
        }

        $this->assertInstanceOf(User::class, $user);

     }

}
