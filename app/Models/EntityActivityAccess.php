<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntityActivityAccess extends Model
{
    use HasFactory;

    protected $table = 'entity_activity_accesses';

    public function users () {
        return $this->belongsToMany(
            User::class,
            'user_entity_activity_access',
            'entity_activity_access_id',
            'user_id');
    }

}
