<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Category;

class CategorySeeder extends Seeder
{
    private $categories = [

        [
            'name' => 'Entity Types',
            'key' => 'entity_types_category',
            'type_id' => 1
        ],

        [
            'name' => 'Activity Types',
            'key' => 'activity_types_category',
            'type_id' => 1
        ],

        [
            'name' => 'Option Types',
            'key' => 'option_types_category',
            'type_id' => 1
        ],

        [
            'name' => 'Category Types Category',
            'key' => 'category_types_category',
            'type_id' => 1
        ],


        [
            'name' => 'App Types',
            'key' => 'app_types_category',
            'type_id' => 1
        ],


        [
            'name' => 'Entry Types',
            'key' => 'entry_types_category',
            'type_id' => 1
        ],


        [
            'name' => 'Dataset Types',
            'key' => 'dataset_types_category',
            'type_id' => 1
        ],


        [
            'name' => 'Activity Types',
            'key' => 'activity_types_category',
            'type_id' => 1
        ],

        [
            'name' => 'Access Level Types',
            'key' => 'access_level_types_category',
            'type_id' => 1
        ],

        [
            'name' => 'Field Types',
            'key' => 'field_types_category',
            'type_id' => 1
        ],

        [
            'name' => 'File Types',
            'key' => 'file_types_category',
            'type_id' => 1
        ],

    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->categories as $item) {

            $category = Category::firstOrCreate(
                ['key' => $item['key']],
                [
                    'key' => $item['key'],
                    'name' => $item['name'],
                    'type_id' => $item['type_id']
                ]
            );

        }

    }
}
