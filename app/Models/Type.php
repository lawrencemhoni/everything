<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;

    /**
     * Filter by category key
     */
    public function scopeFilterByCategoryKey ($query, $catKey) {

       return $query->join('categories', 'categories.id', 'types.category_id')
                    ->where('categories.key', $catKey);

    }
}
