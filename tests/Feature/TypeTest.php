<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Testing\Fluent\AssertableJson;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Type;
use App\Models\Category;

class TypeTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Create Type with Category Key test
     * @test
     */
    public function user_can_create_type_with_category_key () : void
    {

        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();



        $url = 'platform-zero/types/create';


        $data = [
            'name' => 'Insurance Claims',
            'key' => 'insurance_claims',
            'category_key' => $categoryKey
        ];


        $response1 = $this->actingAsTestUser()->postJson($url, $data);

        $response1->assertStatus(201)
                 ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                         ->where('data.key', $data['key'])
                         ->where('data.category_id', $category->id)
                         ->etc()
                );


    }



    /**
     * Create Type with Category Key test
     * @test
     */
    public function user_can_create_type_with_category_id () : void
    {

        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();



        $url = 'platform-zero/types/create';


        $data = [
            'name' => 'Insurance Registration',
            'key' => 'insurance_registration',
            'category_id' => $category->id
        ];


        $response1 = $this->actingAsTestUser()->postJson($url, $data);

        $response1->assertStatus(201)
                 ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                         ->where('data.key', $data['key'])
                         ->where('data.category_id', $category->id)
                         ->etc()
                );


    }



   /**
     * Get Types with Category Key Test
     * @test
     */
    public function user_can_get_types_with_category_key () : void
    {

        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        Type::factory()
            ->count(10)
            ->create([
                'category_id' => $category->id
        ]);


        #dataparams wasn't working so I didn't mind hacking it.
        $url = "/platform-zero/types?category_key={$categoryKey}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ]);
    }

    /**
     * Get Types with Category ID Test
     * @test
     */
    public function user_can_get_types_with_category_id () : void
    {

        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        Type::factory()
            ->count(10)
            ->create([
                'category_id' => $category->id
        ]);


        #dataparams wasn't working so I didn't mind hacking it.
        $url = "/platform-zero/types?category_id={$category->id}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ]);
    }


    /**
     * Get category test
     * @test
     */
    public function user_can_get_type () : void
    {

        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        $type = Type::factory()
                    ->create([
                        'category_id' => $category->id
                    ]);

        $url = "/platform-zero/types/{$type->id}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertStatus(200);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('data.id', $type->id)
                 ->where('data.name', $type->name)
                 ->where('data.key', $type->key)
                 ->where('data.category_id', $type->category_id)
                 ->etc()
        );

    }


    /**
     *Update Type with Category Key Test
     *@test
     */
    public function user_can_update_type_with_category_key () : void
    {
        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        $type = Type::factory()
                    ->create([
                        'category_id' => $category->id
                    ]);

        $url = "/platform-zero/types/{$type->id}/update";

        $data = [
            'name' => 'Cellphone Data',
            'key' => 'cellphone_data',
            'category_key' => $categoryKey
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.name', $data['name'])
                     ->where('data.key', $data['key'])
                     ->where('data.category_id', $category->id)
                     ->etc()
        );

    }




    /**
     *Update Type with Category ID Test
     *@test
     */
    public function user_can_update_type_with_category_id () : void
    {
        $categoryKey = 'entry_types_category';

        $category = Category::select()
                            ->where('key', $categoryKey)
                            ->first();

        $type = Type::factory()
                    ->create([
                        'category_id' => $category->id
                    ]);

        $url = "/platform-zero/types/{$type->id}/update";

        $data = [
            'name' => 'Cellphone Data',
            'key' => 'cellphone_data',
            'category_id' => $category->id
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.name', $data['name'])
                     ->where('data.key', $data['key'])
                     ->where('data.category_id', $category->id)
                     ->etc()
        );

    }







}
