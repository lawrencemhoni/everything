<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Http\Request;

use App\RequestModelManagers\CategoryManager;

use App\Models\Type;
use App\Models\Category;

class CategoryTest extends TestCase
{
    /**
     *Create category with type key test
     *@test
     */
    public function user_can_create_category_with_type_key () : void
    {
        $data = [
            'name' => 'Real Estate',
            'key' => 'real_estate_cat',
            'type_key' => 'entry_category_type',
        ];

        $req = Request::create('/create', 'POST', $data);

        $category = CategoryManager::createCategory($req);

        unset($data['type_key']);

        $this->assertDatabaseHas('categories', $data);

    }


    /**
     *Create category with type id test
     *@test
     */
    public function user_can_create_category_with_type_id () : void
    {
        $type = Type::where('key', 'entry_category_type')->first();

        $data = [
            'name' => 'Real Estate',
            'key' => 'real_estate_cat',
            'type_id' => $type->id,
        ];

        $req = Request::create('/create', 'POST', $data);

        $category = CategoryManager::createCategory($req);


        $this->assertDatabaseHas('categories', $data);

    }

    /**
     *Get categories tests
     *@test
     */
    public function user_can_get_categories () : void
    {

        $type = Type::where('key', 'entry_category_type')->first();

        $parentCategory =  Category::factory()->create([
            'type_id' => $type->id
        ]);


        Category::factory()
                ->count(10)
                ->create([
                    'type_id' => $type->id,
                    'parent_id' => $parentCategory->id
                ]);

        $data = [

            'type_key' => 'entry_category_type'

        ];

        $req = Request::create('/categories', 'GET', $data);

        $categories = CategoryManager::getCategories($req);


        foreach ($categories as $category) {

            $this->assertDatabaseHas('categories', [
                'id' => $category->id,
                'name' => $category->name,
                'type_id' => $category->type_id
            ]);

            foreach ($category->childCategories as $childCategory) {

                $this->assertDatabaseHas('categories', [
                    'id' => $childCategory->id,
                    'name' => $childCategory->name,
                    'type_id' => $childCategory->type_id,
                    'parent_id' => $category->id
                ]);

            }

        }

    }


    /**
     *Get category test
     *@test
     */
    public function user_can_get_category () : void
    {

        $type = Type::where('key', 'entry_category_type')->first();

        $data = [
            'name' => 'Vehicles',
            'key' => 'vehicles_cat',
            'type_id' => $type->id,
        ];

        $category = Category::factory()->create($data);

        $req = Request::create('/category', 'GET');

        $foundCategory = CategoryManager::getCategory($req, $category->id);

        if ($category->id == $foundCategory->id &&
            $category->name == $foundCategory->name &&
            $category->type_id == $foundCategory->type_id) {

            $this->assertTrue(true);

        } else {

            $this->assertTrue(false);

        }

    }

    /**
     *Update category with type key
     *@test
     */
    public function user_can_update_category_with_type_key () : void
    {

        $type = Type::where('key', 'entry_location_category_type')->first();

        $parentCategory =  Category::factory()->create([
            'type_id' => $type->id
        ]);


        $category = Category::factory()->create([
            'type_id' => $type->id
        ]);

        $data = [
            'name' => 'Electronics',
            'key' => 'electronics_cat',
            'type_key' => 'entry_location_category_type',
            'parent_id' => $parentCategory->id
        ];

        $req = Request::create('/create', 'POST', $data);

        $updatedCategory = CategoryManager::updateCategory($req, $category->id);

        if ($category->id == $updatedCategory->id &&
            $category->type_id == $updatedCategory->type_id &&
            $updatedCategory->parent_id == $parentCategory->id ) {

            $this->assertTrue(true);

        } else {

            $this->assertTrue(false);

        }

        unset($data['type_key']);

        $data['type_id'] = $type->id;

        $this->assertDatabaseHas('categories', $data);

    }





    /**
     *Update category with type key
     *@test
     */
    public function user_can_update_category_with_type_id () : void
    {

        $type = Type::where('key', 'entry_category_type')->first();

        $category = Category::factory()->create([
            'type_id' => $type->id
        ]);

        $data = [
            'name' => 'Electronics',
            'key' => 'electronics_cat',
            'type_id' => $type->id
        ];

        $req = Request::create('/create', 'POST', $data);

        $updatedCategory = CategoryManager::updateCategory($req, $category->id);

        if ($category->id == $updatedCategory->id &&
            $category->type_id == $updatedCategory->type_id ) {

            $this->assertTrue(true);

        } else {

            $this->assertTrue(false);

        }

        $this->assertDatabaseHas('categories', $data);

    }








}
