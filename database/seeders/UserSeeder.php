<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\AccessLevel;
use App\Models\EntityActivityAccess;
use App\Models\UserEntityActivityAccess;

use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $accessLevel = AccessLevel::select()
                        ->where('name', 'Superuser')
                        ->firstOrFail();

        $user = User::firstOrCreate(
            ['username' => 'master'],
            [
                'username' => 'master',
                'first_name' => 'master',
                'last_name' => 'master',
                'email' => 'master@site.com',
                'access_level_id' => $accessLevel->id,
                'password' => 'ADX50009'
            ]
        );

        $entityActivityAccesses = EntityActivityAccess::select(DB::raw('DISTINCT entity_activity_accesses.id'))
                                        ->join(
                                            'access_level_entity_activity_accesses',
                                            'access_level_entity_activity_accesses.type_id',
                                            'entity_activity_accesses.type_id')
                                        ->where('access_level_id', $accessLevel->id)
                                        ->get();

        foreach ($entityActivityAccesses as $access) {

            $access->users()->sync([$user->id]);

        }

    }
}
