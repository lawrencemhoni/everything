<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\EntityActivityAccess;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserEntityActivityAccess>
 */
class UserEntityActivityAccessFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $entityAccesAccess = EntityActivityAccess::select();


        return [

            //
        ];
    }
}
