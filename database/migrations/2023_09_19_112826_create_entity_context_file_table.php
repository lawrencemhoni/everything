<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entity_context_file', function (Blueprint $table) {
            $table->id();
            $table->integer('entity_id');
            $table->integer('type_id');
            $table->integer('instance_id');
            $table->integer('file_id');
            $table->tinyInteger('is_for_display');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entity_context_file');
    }
};
