<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Testing\Fluent\AssertableJson;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Entity;
use App\Models\EntityDataset;
use App\Models\Field;

class EntityDatasetTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Create Dataset Test
     * @test
     */
    public function user_can_create_dataset () : void
    {
        $entity = Entity::selectBasics()
                        ->filterByTypeKey('public_entity_type')
                        ->first();

        $url = "platform-zero/entities/{$entity->id}/datasets/create";

        $data = [
            'name' => 'Appliances Data'
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertStatus(201)
                 ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                        ->etc()
                );

    }


     /**
     * Create Entity Dataset with Fields Test
     * @test
     */
    public function user_can_create_entity_dataset_with_fields () : void
    {
        $entity = Entity::selectBasics()
                        ->filterByTypeKey('public_entity_type')
                        ->first();

        $url = "platform-zero/entities/{$entity->id}/datasets/create";

        $data = [
            'name' => 'Appliances Data'
        ];


        #Now we are testing if JSON has fields

        $fields = [
            [
                'name' => 'Title',
                'type_key' => 'text_field_type'
            ],
            [
                'name' => 'Description',
                'type_key' => 'long_text_field_type'
            ]
        ];


        $data['fields'] = $fields;

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertJsonStructure([
            'data' => [
                    'id',
                    'name',
                    'fields' => [
                        [
                            'id',
                            'name',
                            'type_id'
                        ]

                    ]
                ]

        ]);

    }


    /**
     * Get Entity Dataset Test
     * @test
     */
    public function user_can_get_entity_dataset () : void
    {

        $entityDataset = EntityDataset::factory()->create();

        $url = "/platform-zero/entities/$entityDataset->entity_id/datasets/{$entityDataset->id}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('data.id', $entityDataset->id)
                 ->where('data.name', $entityDataset->name)
                 ->etc()
        );

    }




   /**
     * Get Apps Test
     * @test
     */
    public function user_can_get_entity_datasets () : void
    {
        $entity = Entity::selectBasics()
                        ->filterByTypeKey('public_entity_type')
                        ->first();

        EntityDataset::factory()->count(20)->create([
            'entity_id' => $entity->id
        ]);

        $url = "/platform-zero/entities/{$entity->id}/datasets";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJsonStructure([
            'data' => [

                [
                    'id',
                    'name'
                ]
            ]
        ]);
    }


   /**
     *Update Dataset with Field Test
     *@test
     */
    public function user_can_update_dataset () : void
    {

        $entityDataset = EntityDataset::factory()->create();

        $url = "/platform-zero/entities/{$entityDataset->entity_id}/datasets/{$entityDataset->id}/update";

        $data = [
            'name' => 'Members Data',
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertStatus(200)
                  ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                         ->where('data.entity_id', $entityDataset->entity_id)
                         ->etc()
                );
    }





   /**
     *Update Dataset with Field Test
     *@test
     */
    public function user_can_update_dataset_with_fields () : void
    {

        $entityDataset = EntityDataset::factory()->create();

        $url = "/platform-zero/entities/{$entityDataset->entity_id}/datasets/{$entityDataset->id}/update";


        $fields = [
            [
                'name' => 'Title',
                'type_key' => 'text_field_type'
            ],
            [
                'name' => 'Description',
                'type_key' => 'long_text_field_type'
            ]
        ];


        $data = [
            'name' => 'Animal Feed Data',
        ];


        $data['fields'] = $fields;

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertStatus(200)
                ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                        ->where('data.entity_id', $entityDataset->entity_id)
                        ->etc()
                )
                ->assertJsonStructure([
                    'data' => [
                            'id',
                            'name',
                            'fields' => [
                                [
                                    'id',
                                    'name',
                                    'type_id'
                                ]

                            ]
                ]
        ]);

    }



}
