<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Type;
use App\Models\AccessLevel;
use App\Models\AccessLevelEntityActivityAccess;
use App\Models\EntityActivityAccess;

class AccessLevelSeeder extends Seeder
{


    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $accessLevelName = 'Superuser';

        $type = Type::where('key','super_access_level_type')
                    ->first();

        $count = AccessLevel::where('name', $accessLevelName)
                            ->where('type_id', $type->id)
                            ->count();

        if ($count == 0) {

            $accessLevel = new AccessLevel;
            $accessLevel->name = $accessLevelName;
            $accessLevel->type_id = $type->id;
            $accessLevel->save();

            $entityActivityAccesses = EntityActivityAccess::select()->get();

            foreach ($entityActivityAccesses as $access) {

                $accessLevelEntityActivityAccess = new AccessLevelEntityActivityAccess;
                $accessLevelEntityActivityAccess->entity_id = $access->entity_id;
                $accessLevelEntityActivityAccess->type_id = $access->type_id;
                $accessLevelEntityActivityAccess->access_level_id = $accessLevel->id;
                $accessLevelEntityActivityAccess->save();

            }

        }

    }
}
