## About Everything

Everything is an API that allows Developers and non Developers to build Applications by writing Less Code. For now the project is about Architectural and conceptual experiments and discoveries. 


## License

This is not an open source project. It belongs to Lawrence Mhoni.


## Installation



### Dependencies

- Docker and docker-compose


### Things to look of for.

The .env file is ready and the database password is set therein. Will include that here at a later stage.
The password in the db container depends on that configuration.


### Setting up with docker

First clone the repositories into your desired directory

```
git clone https://gitlab.com/lawrencemhoni/everything.git

```

Navigate into the project's directory


```
cd everything

```

Then build the docker images


```
docker-compose build app

```


Once the images have been built successfully run the following:


```
docker-compose up -d

```



You can confirm that the container is up and running by running the following:


```
docker-compose ps

```

### Setting up the Laravel application

Once you have the docker containers up and running while in the project directory, 
Go inside the container by running the following command;


```
docker-compose exec app bash

```

Once you are in the container, install all Laravel dependancies by running the following command.


```
composer install

```



Then generate key by running the following command.


```
php artisan key:generate

```






Once dependancies have been installed, let's do some database migration and install all the necessary data to get the application ready for use.

Run the following.


```
php artisan migrate --seed -seeder=InstallationSeeder && php artisan passport:install

```

You can test the application by running the following command.


```
php artisan test

```
