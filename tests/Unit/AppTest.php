<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;

use App\Models\App;
use App\RequestModelManagers\AppManager;

class AppTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Create user test
     * @test
     */
     public function user_can_create_app () : void
     {
        $data = [
            'name' => 'Peza android',
            'type_key' => 'android_mobile_app_type'
        ];

        $req = Request::create('/create', 'POST', $data);

        $app = AppManager::createApp($req);

        unset($data['type_key']);

        $this->assertDatabaseHas('apps', $data);
     }


     /**
      * Get apps test
      * @test
      */
     public function user_can_get_apps () : void
     {

        App::factory()->count(10)->create();

        $req = Request::create('/apps', 'GET');

        $apps = AppManager::getApps($req);

        foreach ($apps as $app) {
            $this->assertDatabaseHas('apps', [
                'id' => $app->id,
                'name' => $app->name,
                'type_id' => $app->type_id
            ]);
        }

     }

     /**
      * Get app test
      * @test
      */
      public function user_can_get_app () : void
      {
         $app = App::factory()->create();

         $req = Request::create('/app', 'GET');

         $foundApp = AppManager::getApp($req, $app->id);

         if ($foundApp->id == $app->id && $foundApp->name == $app->name) {
            $this->assertTrue(true);
         }

      }

      /**
       * Update app test
       * @test
       */
      public function user_can_update_app () : void
      {
        $oldData = [
            'name' => 'Ndi Deal IOS App'
        ];

        $app = App::factory()->create($oldData);

        $this->assertDatabaseHas('apps', $oldData);


        $newData = $oldData;

        unset($newData['type_id']);

        $newData['name'] = 'All things IOS App';
        $newData['type_key'] = 'ios_mobile_app_type';

        $req = Request::create('/create', 'POST', $newData);

        $newApp = AppManager::updateApp($req, $app->id);

        unset($newData['type_key']);

        $this->assertDatabaseHas('apps', $newData);

        $this->assertDatabaseMissing('apps', $oldData);

      }





}
