<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Exception;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\ForbiddenAccessException;

use App\Services\EntityAccessService;
use App\RequestModelManagers\UserManager;


class UserController extends Controller
{
    public $entityAccess;

    public function __construct (EntityAccessService $entityAccessServices) {
        $this->entityAccess = $entityAccessServices;
    }

    public function createUser (Request $req) {

        try {

            $validated = $req->validate([
                'username' => 'required',
                'email' => 'required|email',
                'first_name' => 'required',
                'last_name' => 'required',
                'password' => 'required',
            ]);

            $this->entityAccess->check('user',
            'create_activity_type');

            $user = UserManager::createUser($req);

            return response()->json(['data' => $user], 201);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        }  catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }

    }


    public function getUsers (Request $req) {

        try {

            $this->entityAccess->check('user',
                  'list_activity_type');

            return UserManager::getUsers($req);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception  $e) {

            \Log::info($e->getMessage());
            return response()->json([], 500);
        }
    }


    public function viewUser (Request $req, $id) {
        try {

            $this->entityAccess->check('user', 'view_activity_type');

            $user = UserManager::getUser($req, $id);

            return response()->json(['data' => $user], 200);


        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception $e ) {
            return response()->json([], 500);
        }
    }


    public function updateUser (Request $req, $id) {

        try {

            $validated = $req->validate([
                'username' => 'required',
                'email' => 'required|email',
                'first_name' => 'required',
                'last_name' => 'required'
            ]);

            $this->entityAccess->check('user',
            'update_activity_type');

            $user =  UserManager::updateUser($req, $id);

            return response()->json(['data' => $user], 200);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException $e ) {
            return response()->json([], 404);
        } catch ( Exception $e ) {
            return response()->json([], 500);
        }

    }
}
